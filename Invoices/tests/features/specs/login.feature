#language: pt

Funcionalidade: Login
    Sendo um usuario
    Posso logar no sistema Invoices
    Para cadastrar clientes e tambem lançar cobraças

Cenario: Acesso 
    * acessa a pagina login

@logout
Cenario: Administrador faz login
    Dado que eu tenho um usuário:
        | email | kato.danzo@qaninja.com.br|
        | senha | secret                   |
    Quando faço login
    Então vejo o dashboard 
        E a mensagem "Bem Vindo Kato Danzo!"
  
        
Esquema do Cenario: Tentativa de login
    Dado que eu tenho o usuario <email> e <senha>
    Quando faço login
    Então vejo a mensagem <msg>

    Exemplos:
        | email                       | senha    | msg                                 |
        | "kato.danzo@qaninja.com.br" | "123456" | "Incorrect password"                |
        | "kato.danzo@qaninja.net"    | "secret" | "User not found"                    |
        | "kato.danzo&qaninja.com.br" | "123456" | "Please enter your e-mail address." |

