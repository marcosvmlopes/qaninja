#language: pt

Funcionalidade: Cadastro de clientes
    Sendo um usuario do Invoices
    Posso cadastrar novos clientes
    Para fins de gerenciamento e também atendimento

Contexto: Acesso a página cliente
   * Usuario logado acessa a pagina clientes

Cenario: Cadastro de novo cliente
    Ao  cadastrar um novo cliente, o mesmo deve 
    ser exibido na busca

    Dado que eu tenha uma lista de clientes 
    Quando faço o cadastro desses clientes
    Então esse cliente devem ser exibido na busca
