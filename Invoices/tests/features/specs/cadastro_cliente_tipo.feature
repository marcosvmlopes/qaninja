#language: pt

Funcionalidade: Cadastro de clientes
    Sendo um usuario do Invoices
    Posso cadastrar novos clientes
    Para fins de gerenciamento e também atendimento

Contexto: Acesso a página cliente
   * Usuario logado acessa a pagina clientes

@tipo
Cenario: Cadastrar qualquer cliente

    Dado que eu tenha um novo cliente
    Quando faço o cadastro desse cliente
    Então esse cliente deve ser exibido na busca