#encoding: utf-8

Dado(/^acessa a pagina login$/) do
  @login.load
end

Dado(/^que eu tenho um usuário:$/) do |table|
  @user = OpenStruct.new(table.rows_hash)
end

Quando(/^faço login$/) do
  @login.with(@user.email, @user.senha)
end

Então(/^vejo o dashboard$/) do
  expect(@nav.user_menu.text).to eql @user.email
end

Então(/^a mensagem "([^"]*)"$/) do |message|
    expect(@dash.title.text).to eql message
end

##Senha invalidas

Dado(/^que eu tenho o usuario "([^"]*)" e "([^"]*)"$/) do |email, senha|
  @user = OpenStruct.new(
    :email => email,
    :senha => senha
  )
end                                                                      

Então(/^vejo a mensagem "([^"]*)"$/) do |message|
  expect(@login.alert.text).to include message                               
end                                                                          