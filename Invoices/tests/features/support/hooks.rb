Before do
  @login = LoginPage.new
  @nav = NavPage.new
  @dash = DashPage.new
  @customer = CustomerPage.new
end  


After('@logout') do
    @nav.logout
    @login.load
end

Before ('@customer_page')do
  @login.load
  @login.with('kato.danzo@qaninja.com.br', 'secret')
  @dash.wait_for_title
  @customer.load
end
