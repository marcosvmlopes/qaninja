#encoding: utf-8

Dado(/^Usuario logado acessa a pagina clientes$/) do                         
    @login.load
    @login.with('kato.danzo@qaninja.com.br', 'secret')
    @dash.wait_for_title
    @customer.load
end                                                                          

Dado(/^que eu tenha uma lista de clientes$/) do
    @tipos = ['Gold', 'Prime', 'Exclusivo', 'Platinum']
    @users = Array.new

    @tipos.each do |t|
        name = "#{Faker::Name.first_name} #{Faker::Name.first_name}"                         
        @users.push(
            OpenStruct.new(
                :nome => name,
                :telefone => Faker::PhoneNumber.cell_phone ,
                :email =>Faker::Internet.free_email(name),
                :tipo => t,
                :obs => Faker::Lorem.paragraph
                )
            )
    end    
end                                                                                                                                                  
                                                                             
Quando(/^faço o cadastro desses clientes$/) do
    @users.each do |u|
        @customer.save(u)
    end                                     
end                                                                          
                                                                             
Então(/^esse cliente devem ser exibido na busca$/) do
    @users.each do |u|
        @customer.search(u.email)
        expect(@customer.view.size).to eql 1
        expect(@customer.view.first.text).to include u.nome
        expect(@customer.view.first.text).to include u.email
        expect(@customer.view.first.text).to include u.telefone
    end    
end      

## tipo
Dado(/^que eu tenha um novo cliente$/) do                                    
    @tipos = ['Gold', 'Prime', 'Exclusivo', 'Platinum']

    name = "#{Faker::Name.first_name} #{Faker::Name.first_name}"                         
    @user =OpenStruct.new(
        :nome => name,
        :telefone => Faker::PhoneNumber.cell_phone ,
        :email =>Faker::Internet.free_email(name),
        :tipo => @tipos.sample,
        :obs => Faker::Lorem.paragraph
    )   
end                                                                          
                                                                             
Quando(/^faço o cadastro desse cliente$/) do                                 
  @customer.save(@user)
end                                                                          
                                                                             
Então(/^esse cliente deve ser exibido na busca$/) do                         
  @customer.search(@user.email)
  expect(@customer.view.size).to eql 1
  expect(@customer.view.first.text).to include @user.nome
  expect(@customer.view.first.text).to include @user.email
  expect(@customer.view.first.text).to include @user.telefone
end

##Delete
Dado(/^Cliente cadastro para remosao$/) do                                   
   @tipos = ['Gold', 'Prime', 'Exclusivo', 'Platinum']

    name = "#{Faker::Name.first_name} #{Faker::Name.first_name}"                         
    @user =OpenStruct.new(
        :nome => name,
        :telefone => Faker::PhoneNumber.cell_phone ,
        :email =>Faker::Internet.free_email(name),
        :tipo => @tipos.sample,
        :obs => Faker::Lorem.paragraph
    )
    @customer.save(@user)   
end                                                                          

Dado(/^que eu tenho um cliente ja cadastrado$/) do
    ## como deixar a massa de dados sem hard coded                           
  @email = @user.email
end                                                                          
                                                                             
Quando(/^faço a exclusão desse cliente$/) do                                 
  @customer.wait_for_view
  @customer.view.each do |tr|
      if (tr.text.include?(@email))
        tr.find('span[class*=trash]').click
        @customer.delete_yes.click
      end
  end    
end                                                                          
                                                                             
Então(/^esse cliente não deve ser exibido na busca$/) do                     
  @customer.search(@email)
  expect(@customer.search_alert.text).to eql "\"#{@email}\" não encontrado." 
end                                                                                                                                                                                                                        