#language: pt

@delete
Funcionalidade: Remover cliente
    Sendo um usuario do Invoices
    Posso remover somente os clientes que ainda não possuem fatura

Contexto: Cadastrar cliente para remosao
   * Cliente cadastro para remosao

@customer_page
Cenario: Remover cliente com sucesso

    Dado que eu tenho um cliente ja cadastrado
    Quando faço a exclusão desse cliente
    Então esse cliente não deve ser exibido na busca