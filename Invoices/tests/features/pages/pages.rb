
#elementos usado no login 
class LoginPage <SitePrism::Page
    set_url '/login'

    element :email, '#email'
    element :password, 'input[placeholder*=senha]'
    element :sign_in, '.login-button'

    element :alert, 'div[class$=alert-warning]'
    
    def with(email, pwd)
        self.email.set email
        self.password.set pwd
        self.sign_in.click
    end    

end

class NavPage <SitePrism::Page
    element :user_menu, '#navbar a[data-toggle=dropdown]'
    element :logout_link, '#navbar a[href$=logout]'

    def logout
         self.user_menu.click
         self.logout_link.click
    end    
end

class DashPage <SitePrism::Page
    element :title, '#page_title'
end

#elementos usados no cadastro de cliente

class CustomerPage <SitePrism::Page
    set_url '/customers'

    element :new_button, '#dataview-insert-button'
    element :name, 'input[name=name]'
    element :phone, 'input[name=phone]'
    element :email, 'input[name=email]'
    element :type, 'select[id=costumer]'
    element :note, 'textarea[name=note]'
    element :save_button, '#form-submit-button'

    element :search_field, 'input[name=search]'
    element :search_button, 'button[id*=search]'
    element :search_alert, '.alert-warning'

    elements :view, 'table tbody tr'

    element :delete_yes, '.modal-content button[data-bb-handler=success]'


    def save(user)
        self.new_button.click
        self.name.set user.nome
        self.phone.set user.telefone
        self.email.set user.email
        self.type.find('option', text: user.tipo).select_option
        #self.type.all('option').sample.select_option
        self.note.set user.obs
        self.save_button.click
    end

    def search(target)
        self.search_field.set target
        self.search_button.click
    end        
end